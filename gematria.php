<?php

/*
1 == 'א׳'
14 == 'י״ד'
15 == 'ט״ו'
114 == 'קי״ד'
115 == 'קט״ו'
*/
$hsn=[  
    "separators"=>
    [
        "geresh"=> '׳',
        "gershayim"=> '״'
    ],
    "numerals"=>
    [
        1=> "א",
        2=> "ב",
        3=> "ג",
        4=> "ד",
        5=> "ה",
        6=> "ו",
        7=> "ז",
        8=> "ח",
        9=> "ט",
        10=> "י",
        20=> "כ",
        30=> "ל",
        40=> "מ",
        50=> "נ",
        60=> "ס",
        70=> "ע",
        80=> "פ",
        90=> "צ",
        100=> "ק",
        200=> "ר",
        300=> "ש",
        400=> "ת",
        500=> "תק",
        600=> "תר",
        700=> "תש",
        800=> "תת",
        900=> "תתק",
    ],
    "specials"=>
    [
        0=> '0',
        15=> "טו",
        16=> "טז",
        115=> "קטו",
        116=> "קטז",
        215=> "רטו",
        216=> "רטז",
        270=> "ער",
        272=> "ערב",
        274=> "עדר",
        275=> "ערה",
        298=> "רחצ",
        304=> "דש",
        315=> "שטו",
        316=> "שטז",
        344=> "שדמ",
        415=> "תטו",
        416=> "תטז",
        515=> "תקטו",
        516=> "תקטז",
        615=> "תרטו",
        616=> "תרטז",
        670=> 'עתר',
        672=> "תערב",
        674=> "עדרת",
        698=> "תרחצ",
        715=> "תשטו",
        716=> "תשטז",
        744=> "תשדמ",
        815=> "תתטו",
        816=> "תתטז",
        915=> "תתקטו",
        916=> "תתקטז",
    ]
];
function hebrew_numeral($val, $gershayim=true)
{
    global $hsn;
    //1. Lookup in specials
    if( array_key_exists( $val, $hsn['specials'] ) )
    {
        $retval = $hsn['specials'][$val];
        if( $gershayim )
        {
            return add_gershayim($retval);
        }else
        {
            return $retval;
        }
    }
    // 2. Generate numeral normally
    $parts = [];
    $rest = strval($val);
    while( $rest )
    {
        $digit = intval(substr($rest, 0, 1));
        $rest = substr( $rest, 1 );
        if( $digit == 0 )
        {   
            continue;
        }
        $power = pow( 10, strlen($rest) );
        array_push( $parts, $hsn['numerals'][$power * $digit] );
    }
    $retval = implode( '', $parts );
    // 3. Add gershayim
    if( $gershayim )
    {
        return add_gershayim( $retval );
    }else
    {
        return $retval;
    }
}

function add_gershayim($s)
{
    global $hsn;
    $temp = $s;
    if( mb_strlen($temp) == 1 )
    {
        $s .= $hsn['separators']['geresh'];
    }else
    {
        $s = mb_substr($temp, 0, -1) . $hsn['separators']['gershayim'] . mb_substr($temp, -1);
    }
    return $s;
}
/*
print(hebrew_numeral(1, true)."<br>");
print(hebrew_numeral(14, true)."<br>");
print(hebrew_numeral(15, true)."<br>");
print(hebrew_numeral(114, true)."<br>");
print(hebrew_numeral(115, true)."<br>");
*/


?>
