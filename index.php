<html>

<head>
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="https://fonts.googleapis.com/css2?family=Noto+Serif+Hebrew:wght@800&display=swap" rel="stylesheet">
		<style>
			#thilim {
				font-family: 'Noto Serif Hebrew';font-size: 32px;
			}
		</style>
</head>

<body dir=rtl>
<?php
require_once("gematria.php");

//ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);


$month = date("n");
$day = date("j");
$year = date("Y");

$hebrewDate = jdtojewish(gregoriantojd($month, $day, $year));
list($hebrewMonth, $hebrewDay, $hebrewYear) = preg_split('|/|',$hebrewDate);



$month_array[1] = [1,9];
$month_array[2] = [10,17];
$month_array[3] = [18,22];
$month_array[4] = [23,28];
$month_array[5] = [29,34];
$month_array[6] = [35,38];
$month_array[7] = [39,43];
$month_array[8] = [44,48];
$month_array[9] = [49,54];
$month_array[10] = [55,59];
$month_array[11] = [60,65];
$month_array[12] = [66,68];
$month_array[13] = [69,71];
$month_array[14] = [72,76];
$month_array[15] = [77,78];
$month_array[16] = [79,82];
$month_array[17] = [83,87];
$month_array[18] = [88,89];
$month_array[19] = [90,96];
$month_array[20] = [97,103];
$month_array[21] = [104,105];
$month_array[22] = [106,107];
$month_array[23] = [108,112];
$month_array[24] = [113,118];
$month_array[25] = [119,119];
$month_array[26] = [119,119];
$month_array[27] = [120,134];
$month_array[28] = [135,139];
$month_array[29] = [140,144];
$month_array[30] = [145,150];

$todays_day = $hebrewDay;
//$todays_day = 26;

$hebrew_todays_day = hebrew_numeral($todays_day, true);
echo "$hebrew_todays_day of the month\n<br>";
$hebrew_perek_from = hebrew_numeral($month_array[$todays_day][0]);
$hebrew_perek_to = hebrew_numeral($month_array[$todays_day][1]);
echo "$hebrew_perek_from to $hebrew_perek_to\n";

$file1_text = file_get_contents('Psalms - he - Tanach with Nikkud.json');
$file1_json = json_decode($file1_text, true);

$file2_text = file_get_contents('Rashi on Psalms - he - On Your Way.json');
$file2_json = json_decode($file2_text, true);

$file3_text = file_get_contents('Metzudat David on Psalms - he - On Your Way.json');
$file3_json = json_decode($file3_text, true);

$file4_text = file_get_contents('Metzudat Zion on Psalms - he - On Your Way.json');
$file4_json = json_decode($file4_text, true);

for($start_perek = $month_array[$todays_day][0]; $start_perek <= $month_array[$todays_day][1]; $start_perek++)
{
	$unix_perek = $start_perek - 1;
	$hebrew_start_perek = hebrew_numeral($start_perek, true);
	echo "<center>\n<hr>\n<br>$hebrew_start_perek\n</center>";
	$File1_Psukim = $file1_json['text'][$unix_perek];
	$count_Posuk = 0;
	foreach($File1_Psukim as $File1_key => $File1_Posuk)
	{


		//skip 119 halfs
		if($todays_day == 25)
		{
			if($count_Posuk > 95)
				break;
		}
		if($todays_day == 26)
		{
			if($count_Posuk < 96)
			{
				++$count_Posuk;
				continue;
			}
		}
		
		$hebrew_count_Posuk = hebrew_numeral($count_Posuk + 1, true);
		echo "\n<br><span style='background-color: black; color: white;'>&nbsp;$hebrew_count_Posuk&nbsp;</span> <b><span id=thilim>$File1_Posuk</span></b>\n<br>";
		//print_r($file2_json['text']);
		echo "<font color=red>";
		foreach($file2_json['text'][$unix_perek][$count_Posuk] as $file2_comment)
		echo "$file2_comment\n";
		echo "</font>\n";

		echo "<font color=blue>";
		foreach($file3_json['text'][$unix_perek][$count_Posuk] as $file3_comment)
		echo "$file3_comment\n";
		echo "</font>";

		echo "<font color=green>";
		foreach($file4_json['text'][$unix_perek][$count_Posuk] as $file4_comment)

		echo "$file4_comment\n";
		echo "</font>";
		++$count_Posuk;

	}

	++$count_Perek;


}
